<?php
/**
 * The declaration of MCL_TEMPLATE_GENOTYPE_LONG_FORM_BIMS class.
 *
 */
class MCL_TEMPLATE_GENOTYPE_LONG_FORM_BIMS extends MCL_TEMPLATE {

  /**
   *  Class data members.
   */
  /**
   * @see MCL_TEMPLATE::__construct()
   */
  public function __construct($details = array()) {
    $details['template_type'] = 'GENOTYPE';
    parent::__construct($details);
  }

  /**
   * @see MCL_TEMPLATE::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
  }

  /**
   * @see MCL_TEMPLATE::defineDescription()
   */
  public function defineDescription() {
    $desc = "
      This is the template to upload SNP genotyping data. The marker names are listed as headers with prefix '$'.
      There are no place to specify the organism for the markers. So this template is used only when marker names
      are absolutely unique in the feature table.
    ";
    return $desc;
  }

  /**
   * @see MCL_TEMPLATE::defineHeaders()
   */
  public function defineHeaders() {
    $headers = array(
      'dataset_name'  => array('req' => TRUE,  'width' => 10, 'desc' => "Name of the genotyping dataset. It should match a dataset_name in 'Dataset' sheet."),
      'accession'     => array('req' => TRUE,  'width' => 15, 'var' => TRUE, 'desc' => "ID of the accession that has been phenotyped. It should match an 'accession' column entry in the 'Accession' sheet or 'progeny_name' column entry 'Progeny' sheet."),
      'genus'         => array('req' => FALSE, 'width' => 10, 'desc' => "Genus of the stock"),
      'species'       => array('req' => FALSE, 'width' => 10, 'desc' => "Species of the stock"),
      'marker_name'   => array('req' => FALSE, 'width' => 10, 'desc' => "The name of marker"),
      'genotype'      => array('req' => TRUE,  'width' => 10, 'desc' => "Type the alleles with '|' in between (e.g. A|T)"),
      'evaluator'     => array('req' => FALSE, 'width' => 10, 'desc' => "Person who did the genotyping. It should match 'contact_name' of the Contact sheet."),
    );
    return $headers;
  }

  /**
   * @see MCL_TEMPLATE::defineCvterms()
   */
  public function defineCvterms() {
    $cvterms = array();
    $cvterms['sequence']['genetic_marker']  = -1;
    $cvterms['sequence']['SNP']             = -1;
    return $cvterms;
  }

  /**
   * @see MCL_TEMPLATE::runErrorCheckDataLine()
   */
  public function runErrorCheckDataLine($line) {}

  /**
   * @see MCL_TEMPLATE::uploadDataLine()
   */
  public function uploadDataLine($line) {

    // Gets the job paremters.
    $program_id = $this->getJob()->getParamByKey('program_id');

    // Gets the prefix.
    $prefix = BIMS_PROGRAM::getPrefixByID($program_id);

    // Gets the BIMS columns.
    $bims_program = BIMS_PROGRAM::byID($program_id);
    $bims_cols    = $bims_program->getBIMSCols();

    // Gets BIMS_CHADO tables.
    $bc_gc = new BIMS_CHADO_GENOTYPE_CALL($program_id);

    // the reference table.
    $ref_table = $GLOBALS['ref_table'];

    // Adds a genotype into genotype_call.
    if ($line['genotype']) {
      $dataset_name   = $line['dataset_name'];
      $accession_name = $line[strtolower($bims_cols['accession'])];
      $marker_name    = $line['marker_name'];
      $contact_name   = $line['evaluator'];

      // Adds a genotype.
      $details = array(
        'project_id'  => $ref_table['dataset'][$dataset_name],
        'stock_id'    => $ref_table['accession'][$accession_name],
        'feature_id'  => $ref_table['marker'][$marker_name],
        'contact_id'  => $ref_table['contact'][$contact_name],
        'value'       => $line['genotype'],
      );
      $bc_gc->addData($this, $details);
    }
  }

  /**
   * @see MCL_TEMPLATE::preProcess()
   */
  public function preProcess($data_lines) {

    // Gets the job paremters.
    $program_id = $this->getJob()->getParamByKey('program_id');

    // Gets the prefix.
    $prefix = BIMS_PROGRAM::getPrefixByID($program_id);

    // Gets the BIMS columns.
    $bims_program = BIMS_PROGRAM::byID($program_id);
    $bims_cols    = $bims_program->getBIMSCols();

    // Gets BIMS_CHADO tables.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
    $bc_project   = new BIMS_CHADO_PROJECT($program_id);
    $bc_feature   = new BIMS_CHADO_FEATURE($program_id);

    // Go through data and gets the following info.
    // project    : project_id
    // marker     : feature_id
    // accessions : stock_id
    // contact    : contact_id
    //
    $ref_table = array(
      'dataset' => array(),
      'accession' => array(),
      'marker' => array(),
      'contact' => array(),
    );
    foreach ($data_lines as $data_line) {
      $line_no = $data_line['line_no'];

      // Get project_id of dataset_name.
      $dataset_name = $data_line['dataset_name'];
      if (!array_key_exists($dataset_name, $ref_table['dataset'])) {
        $ref_table['dataset'][$dataset_name] = NULL;
        $dataset = $bc_project->byTKey('project', array('name' => $prefix . $dataset_name));
        if ($dataset) {
          $ref_table['dataset'][$dataset_name] = $dataset->project_id;
        }
        else {
          $this->updateMsg('E', "$dataset_name does not exist in project table at $line_no");
        }
      }

      // Get stock_id of accession.
      $accession_name = $data_line[strtolower($bims_cols['accession'])];
      if (!array_key_exists($accession_name, $ref_table['accession'])) {
        $ref_table['accession'][$accession_name] = NULL;
        $accession = $bc_project->byTKey('accession', array('uniquename' => $prefix . $accession_name));
        if ($accession) {
          $ref_table['accession'][$accession_name] = $accession->stock_id;
        }
        else {
          $this->updateMsg('E', "$accession_name does not exist in accession table at $line_no");
        }
      }

      // Get feature_id of marker.
      $marker_name = $data_line['marker_name'];
      if (!array_key_exists($marker_name, $ref_table['marker'])) {
        $ref_table['marker'][$marker_name] = NULL;
        $marker = $bc_feature->byTKey('feature', array('uniquename' => $prefix . $marker_name));
        if ($marker) {
          $ref_table['marker'][$marker_name] = $marker->feature_id;
        }
        else {
          $this->updateMsg('E', "$marker_name does not exist in feature table at $line_no");
        }
      }

      // Get contact_id of contact.
      $contact_name = $data_line['evaluator'] ? $data_line['evaluator'] : 'bims.anonymous';
      if (!array_key_exists($contact_name, $ref_table['contact'])) {
        $ref_table['contact'][$contact_name] = NULL;
        $contact = MCL_CHADO_CONTACT::byName($contact_name);
        if ($contact) {
          $ref_table['contact'][$contact_name] = $contact->getContactID();
        }
        else {
          $this->updateMsg('E', "$contact does not exist in contact table at $line_no");
        }
      }
    }

    // Keeps the reference table in $GLOBAL.
    $GLOBALS['ref_table'] = $ref_table;

    // Skips the error checking process.
    $this->setPassed('syntax error', TRUE);
    $this->setPassed('data error', TRUE);
  }

  /**
   * @see MCL_TEMPLATE::validColumns()
   */
  public function validColumns() {

    // Gets BIMS_PROGRAM.
    $program_id = $this->getJob()->getParamByKey('program_id');
    $bims_program = BIMS_PROGRAM::byID($program_id);

    // Returns FB required columns.
    return array_values($bims_program->getBIMSCols());
  }
}