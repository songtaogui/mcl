[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1421917.svg)](https://doi.org/10.5281/zenodo.1421917)

## Project
MCL (Mainlab Chado Loader) is a module that enables users to upload their biological data to chado database schema. Users are required to transfer their biological data into various types of data template files. MCL, then, uploads these data template files into a chado schema. MCL requires each type of data template to have a corresponding class file. MCL predefines the class files for major data template types such as marker, QTL, germplasm, map, project. The flexibility of Chado schema often allows, however, the same type of biological data to be stored in various ways.. When their data are modeled and stored differently in Chado, users can modify these predefined class files. Otherwise users can inherit the existing template class. MCL also allow for users to define a new class file for a new data template. The details of adding a new template are described in "Customization" section.

MCL provides a "Data Template" page that shows the list of all the types of data template that MCL currently supports. A user can view and download each data template file from the list. MCL also provides "Upload Data" page. See more details in "How to upload data" section below. These two pages are linked from the main page of MCL (http://your.site/mcl).

The Mainlab Chado Loader is created by Main Bioinformatics Lab (Main Lab) at Washington State University. Information about the Main Lab can be found at: https://www.bioinfo.wsu.edu

## Requirement
 - Drupal 7.x

## Version
1.0.0

## Download
The MainLab Chado Loader module can be download from GitLab:

https://gitlab.com/mainlabwsu/mcl

## Installation
After downloading the module, extract it into your site's module directory 
(e.g. sites/all/modules) then follow the instructions below:

  1. Enable the module by using the Drupal administrative interface: Go to: Modules, check 'mcl' (under the MCL) and save or by using the 'drush' command:
     ```
     drush pm-enable mcl
     ```
     This will create all MCL related tables in public schema and populate the tables with default values. It will also create directories for MCL in Drupal public file directory.
